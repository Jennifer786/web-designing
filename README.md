[CodeRepublics](https://www.coderepublics.com) is an online platform to learn web development and designing related technologies. 
It has very simple user interface which helps the user to navigate through it easily and have very simple and effective explanation 
of concepts with examples given for every topic to help users quickly understand the topic. 

Following are the Tutorials are Available on the Website.
[HTML tutorial](https://www.coderepublics.com/HTML/html-tutorial.php) | [CSS Tutoria](https://www.coderepublics.com/CSS/css-tutorial.php) | 
[JavaScript Tutorial](https://www.coderepublics.com/JavaScript/JavaScript-tutorial.php) | [Wordpress tutorial](https://www.coderepublics.com/Wordpress/wordpress-tutorial.php) |
[PHP Tutorial](https://www.coderepublics.com/PHP/php-tutorial.php) |[Pure CSS](https://www.coderepublics.com/Purecss/Purecss-tutorial.php)